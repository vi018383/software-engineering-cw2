# Learning Plan


#### ModuleCode: CS1SE20 
#### Assignment report Title: Models Report
#### Student Number: 29018383 
#### Date (when the work completed): 13/11/2020
#### Actual hrs spent for the assignment: ~18 Hours
#### Assignment evaluation (3 key points): 
- This assignment provided a good practice to use graphs, flowcharts and tables in Gitlab.
- The introduction to use the mermaid syntax was useful to generate charts at a fast pace.
- The assignment enable me have a better underunderstanding of the degree programme. 

## Introduction
This file contains the learning plan for the BSc Computer Science degree programme. It includes the WBS for the entire degree and it provides a breakdown until the module composition. The work progress in outlined in the Gantt chart to identify the milestones for in the course. It also includes a schedule for independent study based on the recommended study hours in the module specification.

The purpose of this file is to support me in having a good understand of the degree programme, and to have information on what modules are being taught in the following years to come. It will also support me to be prepared for the milestones in the years with the aid of the Gantt chart. The work schedule produced will support me to be on track with my studies and complete the assignments / course works on time. 

## Background
The BSc Computer Science programme provides us with the theoretical knowledge and practical application of modern computing in order to become professional software engineers or computer systems engineer. The degree programme is accredited by the British Computer Society, the chartered institute for IT. 

In part 1 of the course we are introduced to the foundation of computer science. It contains 5 compulsory modules: Application of CS, Fundamentals of CS, Programming C/C++, Mathematics & Computation, and Software Engineering. The final module is an optional module, and I have selected the Students Enterprise module. 

The second year of the course contains the core subject content. The compulsory modules are Algorithms and Operating Systems, Computer Architecture and Networking, Compilers, Databases and Information Security, Programming in Java and Software System Design with UML. The final module is an optional module which is to be selected at the beginning of the year. 

The Final year of the course directs us towards our future career in the industry, as we have the opportunity to select our modules, here we can direct ourselves towards a career at this stage, with the aid of the modules that we select. The compulsory modules are Individual Project and Social, Legal and Ethical Aspects of Computing which are 40 and 10 credits respectively. The remaining optional modules are selected at the start of the year which must add up to 70 credits. 

### Work Breakdown Structure
![image.png](./image.png)
Figure 1: BSc Computer Science Degree WBS

Figure 1 represents the WBS for the BSc Computer Science degree course with industrial year. The breakdown shows the work in 4 levels, it shows the year, the modules, the module components, and the topics to be studied in each module.  The optional modules can vary from year to year, I have selected those at the moment, but if there are changes in the optional modules then I must update the WBS.  


There are various methods to create the WBS, such as, in Microsoft PowerPoint, LibreOffice, PlantUML, the mermaid syntax and others. I have experimented with each one of these four to select the best option to use. My preferred method of creating a WBS is in PlantUML, as it is really easy to use and it produces the final result very neatly, however, I was unable to use this method as I had restricted access to the Admin Area in Gitlab in order to integrate PlantUML into Gitlab, thus I did not use this method. Then I tried using the mermaid syntax; the mermaid syntax was very easy to create a WBS, this produced neat result for a small project, and as the project became larger it would not fit the page correctly and it was difficult to read. Then I used Microsoft PowerPoint to create the WBS and I embedded the image in the markdown. Using Microsoft PowerPoint to create a large WBS can be very time consuming as I had to create many boxes and connect them individually, other than time, it also placed strain on the eyes which can damage the eyes if this method is constantly used. 

### Gantt Chart
[![](https://mermaid.ink/img/eyJjb2RlIjoiZ2FudHRcbiAgICB0aXRsZSBCU2MgQ2NvbXB1dGVyIFNjaWVuY2UgR2FudHQgQ2hhcnRcbiAgICBkYXRlRm9ybWF0ICBZWVlZLU1NLUREXG4gICAgc2VjdGlvbiBBcHBsaWNhdGlvbiBvZiBDU1xuICAgIEFydGlmaWNpYWwgSW50ZWxsaWdlbmNlICA6MjAyMC0wOS0yOCwgMzVkXG4gICAgT25saW5lIFRlc3QgMSAgICAgOiAyMDIwLTEwLTI2ICwgMTRkXG4gICAgQ29tcHV0ZXIgVmlzaW9uICA6IDIwMjAtMTEtMDksIDM1ZFxuICAgIE9ubGluZSBUZXN0IDIgICAgIDogMjAyMC0xMi0wNywgMTRkXG4gICAgUm9ib3RpY3MgYW5kIFZpcnR1YWwgUmVhbGl0eTogMjAyMS0wMS0xMSAsIDM1ZFxuICAgIFN1bW1hdGl2ZSBBc2Vzc21lbnQgVEJBOiBcbiAgICBEYXRhIEFuYWx5dGljczogMjAyMS0wMi0yMiAsIDM1ZFxuICAgIFN1bW1hdGl2ZSBBc2Vzc21lbnQgVEJBOiBcbiBcbiAgICBzZWN0aW9uIEZ1bmRhbWVudGFscyBvZiBDU1xuICAgIEluZm9ybWF0aW9uIFJlcHJlc2VudGF0aW9uIGFuZCBEYXRhIG1hbmlwdWxhdGlvbiAgOjIwMjAtMDktMjgsIDM1ZFxuICAgIE9ubGluZSBUZXN0IDEgICAgIDogMjAyMC0xMC0yNiAsIDFkXG4gICAgT1MgYW5kIENvbXB1dGVyIE5ldHdvcmtzIDogMjAyMC0xMS0wOSwgMzVkXG4gICAgTGFiIFJlcG9ydCAxICAgICAgOiAyMDIwLTExLTA5LCAyNmRcbiAgICBPbmxpbmUgVGVzdCAyICAgICA6IDIwMjAtMTItMDcsIDFkICAgIGFub3RoZXIgdGFzayAgICAgIDogMjRkXG4gICAgSW5mb3JtYXRpb24gU2VjdXJpdHkgJiBBbGdvcml0aG1zOiAyMDIxLTAxLTExICwgMzVkXG4gICAgUHJvZ3JhbW1pbmcgbGFuZ3VhZ2U6IDIwMjEtMDItMjIgLCAzNWRcbiAgICBMYWIgUmVwb3J0IDIgOiAyMDIxLTAyLTIyLCAyNmQgICBcbiAgICBcblxuXG4gICAgc2VjdGlvbiBNYXRocyBhbmQgQ29tcHV0aW5nXG4gICAgQWxnZWJyYSwgRnVuY3Rpb25zLCBHcmFwaHMgIDoyMDIwLTA5LTI4LCAzNWRcbiAgICBNYXRyaWNlcyBhbmQgdmVjdG9ycyA6IDIwMjAtMTEtMDksIDM1ZFxuICAgIExhYiBSZXBvcnQgMSAgICAgIDogMjAyMC0xMC0yOCwgNDJkXG4gICAgQ2FsY3VsdXM6IDIwMjEtMDEtMTEgLCAzNWRcbiAgICBDb21wbGV4IG51bWJlcnMsIFByb2IuICYgU3RhdHM6IDIwMjEtMDItMjIgLCAzNWRcbiAgICBMYWIgUmVwb3J0IDIgOiAyMDIxLTAxLTI4LCA0MmQgICAgICAgIFxuXG5cbiAgICBzZWN0aW9uIFByb2dyYW1taW5nIFxuICAgIEZ1bmRhbWVudGFscyBvZiBwcm9ncmFtbWluZyBhbmQgdGhlIEMtbGFuZ3VhZ2UgIDoyMDIwLTA5LTI4LCAzNWRcbiAgICBPbmxpbmUgVGVzdCAxICAgICA6IDIwMjAtMTAtMjYgLCAxZFxuICAgIEZ1bmRhbWVudGFscyBvZiBwcm9ncmFtbWluZyBhbmQgdGhlIEMtbGFuZ3VhZ2UgOiAyMDIwLTExLTA5LCAzNWRcbiAgICBEZXZlbG9wbWVudCBQcm9qZWN0IDogMjAyMC0xMS0wOSwgMzVkXG4gICAgT25saW5lIFRlc3QgMiAgICAgOiAyMDIwLTEyLTExLCAxZCAgICBcbiAgICBPYmplY3Qtb3JpZW50ZWQgcHJvZ3JhbW1pbmcgYW5kIHRoZSBDKysgbGFuZ3VhZ2U6IDIwMjEtMDEtMTEgLCAzNWRcbiAgICBPYmplY3Qtb3JpZW50ZWQgcHJvZ3JhbW1pbmcgYW5kIHRoZSBDKysgbGFuZ3VhZ2U6IDIwMjEtMDItMjIgLCAzNWRcbiAgICBPbmxpbmUgVGVzdCAzIDogMjAyMS0wMy0yNiwgMWQgICBcbiAgICBcbiAgICBzZWN0aW9uIFNvZnR3YXJlIEVuZ2luZWVyaW5nXG4gICAgSW50cm8sIEZvdW5kYXRpb24gYW5kIFF1YWxpdHkgZHJpdmVycywgIDoyMDIwLTA5LTI4LCAzNWRcbiAgICBDb3Vyc2UgV29yayAxOiAyMDIwLTEwLTA5LCAxNGQgXG4gICAgU0UgTW9kZWxzICYgTWV0aG9kcywgUHJvY2Vzc2VzICYgTWFuYWdlbWVudCwgYW5kIFJlcXVpcmVtZW50cyA6IDIwMjAtMTEtMDksIDM1ZFxuICAgIENvdXJzZSBXb3JrIDIgOiAyMDIwLTEwLTMwLCA0MmQgXG4gICAgVGVzdGluZywgRGVzaWduOiAyMDIxLTAxLTExICwgMzVkXG4gICAgQ29uc3RydWN0aW9uLCBNYWludGVuYW5jZSBhbmQgRGVjb21taXNzaW9uaW5nOiAyMDIxLTAyLTIyICwgMzVkXG4gICAgU3VtbWF0aXZlIEFzZXNzbWVudCBUQkE6IGAgICAgXG5cbiAgICBzZWN0aW9uIFN0dWRlbnQgRW50ZXJwcmlzZVxuICAgIEVudHJlcHJlbmV1cmlhbCBtaW5kLXNldCBhbmQgcHJvY2VzcyAgOjIwMjAtMDktMjgsIDdkXG4gICAgSWRlYXRpb24gYW5kIGRlc2lnbiB0aGlua2luZyA6IDIwMjAtMTAtMDUsIDdkXG4gICAgRWZmZWN0dWF0aW9uIDogMjAyMC0xMC0xMiwgN2QgXG4gICAgQnVzaW5lc3MgbW9kZWxzLCBCdXNpbmVzcyBtb2RlbCBjYW52YXMsIGJ1c2luZXNzIG1vZGVsIHBhdHRlcm5zOiAyMDIwLTEwLTE5ICwgN2RcbiAgICBWaWFiaWxpdHksIGRlc2lyYWJpbGl0eSBhbmQgZmVhc2liaWxpdHk6IDIwMjAtMTAtMjYgLCA3ZFxuICAgIExlYW4gc3RhcnQtdXAsIHZhbGlkYXRpb24gZXhwZXJpbWVudHMsIGN1c3RvbWVyIHJlc2VhcmNoOiAyMDIwLTExLTA5XG4gICAgUHJvamVjdC1tYW5hZ2VtZW50IGFuZCB0ZWFtLW1hbmFnZW1lbnQ6IDIwMjAtMTEtMTZcbiAgICBDYXNoZmxvdywgY29zdCAmIHByb2ZpdCwgcHJpY2luZyBzdHJhdGVnaWVzOiAyMDIwLTExLTIzXG4gICAgUHJlc2VudGluZyB0aGUgYnVzaW5lc3MgaWRlYSA6IDIwMjAtMTEtMzBcbiAgICBGaW5hbmNpbmcgZm9yIHN0YXJ0LXVwczogMjAyMC0xMi0wN1xuXG4gICAgRGV2ZWxvcCBCdXNpbmVzcyBQbGFuOiAyMDIwLTEwLTE2LCAxMjBkYFxuICAgIEV4YW0gOiAyMDIxLTAxLTE4LCAxZCBcblxuICAgIFxuICAgIHNlY3Rpb24gRXhhbVxuICAgIEV4YW0gcGVyaW9kIDogMjAyMS0wNS0xMCwgNDVkXG4gICAgVGhlIGZpcnN0IGZpdmUgbW9kdWxlIGFyZSBhc3Nlc3NlZCBpbiB0aGlzIHBlcmlvZDogLlxuICAgIFNwZWNpZmljIGV4YW0gZGF0ZXMgYXJlIFRCQTogLiBcbiIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0IiwidGhlbWVWYXJpYWJsZXMiOnsiYmFja2dyb3VuZCI6IndoaXRlIiwicHJpbWFyeUNvbG9yIjoiI0VDRUNGRiIsInNlY29uZGFyeUNvbG9yIjoiI2ZmZmZkZSIsInRlcnRpYXJ5Q29sb3IiOiJoc2woODAsIDEwMCUsIDk2LjI3NDUwOTgwMzklKSIsInByaW1hcnlCb3JkZXJDb2xvciI6ImhzbCgyNDAsIDYwJSwgODYuMjc0NTA5ODAzOSUpIiwic2Vjb25kYXJ5Qm9yZGVyQ29sb3IiOiJoc2woNjAsIDYwJSwgODMuNTI5NDExNzY0NyUpIiwidGVydGlhcnlCb3JkZXJDb2xvciI6ImhzbCg4MCwgNjAlLCA4Ni4yNzQ1MDk4MDM5JSkiLCJwcmltYXJ5VGV4dENvbG9yIjoiIzEzMTMwMCIsInNlY29uZGFyeVRleHRDb2xvciI6IiMwMDAwMjEiLCJ0ZXJ0aWFyeVRleHRDb2xvciI6InJnYig5LjUwMDAwMDAwMDEsIDkuNTAwMDAwMDAwMSwgOS41MDAwMDAwMDAxKSIsImxpbmVDb2xvciI6IiMzMzMzMzMiLCJ0ZXh0Q29sb3IiOiIjMzMzIiwibWFpbkJrZyI6IiNFQ0VDRkYiLCJzZWNvbmRCa2ciOiIjZmZmZmRlIiwiYm9yZGVyMSI6IiM5MzcwREIiLCJib3JkZXIyIjoiI2FhYWEzMyIsImFycm93aGVhZENvbG9yIjoiIzMzMzMzMyIsImZvbnRGYW1pbHkiOiJcInRyZWJ1Y2hldCBtc1wiLCB2ZXJkYW5hLCBhcmlhbCIsImZvbnRTaXplIjoiMTZweCIsImxhYmVsQmFja2dyb3VuZCI6IiNlOGU4ZTgiLCJub2RlQmtnIjoiI0VDRUNGRiIsIm5vZGVCb3JkZXIiOiIjOTM3MERCIiwiY2x1c3RlckJrZyI6IiNmZmZmZGUiLCJjbHVzdGVyQm9yZGVyIjoiI2FhYWEzMyIsImRlZmF1bHRMaW5rQ29sb3IiOiIjMzMzMzMzIiwidGl0bGVDb2xvciI6IiMzMzMiLCJlZGdlTGFiZWxCYWNrZ3JvdW5kIjoiI2U4ZThlOCIsImFjdG9yQm9yZGVyIjoiaHNsKDI1OS42MjYxNjgyMjQzLCA1OS43NzY1MzYzMTI4JSwgODcuOTAxOTYwNzg0MyUpIiwiYWN0b3JCa2ciOiIjRUNFQ0ZGIiwiYWN0b3JUZXh0Q29sb3IiOiJibGFjayIsImFjdG9yTGluZUNvbG9yIjoiZ3JleSIsInNpZ25hbENvbG9yIjoiIzMzMyIsInNpZ25hbFRleHRDb2xvciI6IiMzMzMiLCJsYWJlbEJveEJrZ0NvbG9yIjoiI0VDRUNGRiIsImxhYmVsQm94Qm9yZGVyQ29sb3IiOiJoc2woMjU5LjYyNjE2ODIyNDMsIDU5Ljc3NjUzNjMxMjglLCA4Ny45MDE5NjA3ODQzJSkiLCJsYWJlbFRleHRDb2xvciI6ImJsYWNrIiwibG9vcFRleHRDb2xvciI6ImJsYWNrIiwibm90ZUJvcmRlckNvbG9yIjoiI2FhYWEzMyIsIm5vdGVCa2dDb2xvciI6IiNmZmY1YWQiLCJub3RlVGV4dENvbG9yIjoiYmxhY2siLCJhY3RpdmF0aW9uQm9yZGVyQ29sb3IiOiIjNjY2IiwiYWN0aXZhdGlvbkJrZ0NvbG9yIjoiI2Y0ZjRmNCIsInNlcXVlbmNlTnVtYmVyQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvciI6InJnYmEoMTAyLCAxMDIsIDI1NSwgMC40OSkiLCJhbHRTZWN0aW9uQmtnQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvcjIiOiIjZmZmNDAwIiwidGFza0JvcmRlckNvbG9yIjoiIzUzNGZiYyIsInRhc2tCa2dDb2xvciI6IiM4YTkwZGQiLCJ0YXNrVGV4dExpZ2h0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0RGFya0NvbG9yIjoiYmxhY2siLCJ0YXNrVGV4dE91dHNpZGVDb2xvciI6ImJsYWNrIiwidGFza1RleHRDbGlja2FibGVDb2xvciI6IiMwMDMxNjMiLCJhY3RpdmVUYXNrQm9yZGVyQ29sb3IiOiIjNTM0ZmJjIiwiYWN0aXZlVGFza0JrZ0NvbG9yIjoiI2JmYzdmZiIsImdyaWRDb2xvciI6ImxpZ2h0Z3JleSIsImRvbmVUYXNrQmtnQ29sb3IiOiJsaWdodGdyZXkiLCJkb25lVGFza0JvcmRlckNvbG9yIjoiZ3JleSIsImNyaXRCb3JkZXJDb2xvciI6IiNmZjg4ODgiLCJjcml0QmtnQ29sb3IiOiJyZWQiLCJ0b2RheUxpbmVDb2xvciI6InJlZCIsImxhYmVsQ29sb3IiOiJibGFjayIsImVycm9yQmtnQ29sb3IiOiIjNTUyMjIyIiwiZXJyb3JUZXh0Q29sb3IiOiIjNTUyMjIyIiwiY2xhc3NUZXh0IjoiIzEzMTMwMCIsImZpbGxUeXBlMCI6IiNFQ0VDRkYiLCJmaWxsVHlwZTEiOiIjZmZmZmRlIiwiZmlsbFR5cGUyIjoiaHNsKDMwNCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGUzIjoiaHNsKDEyNCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIiwiZmlsbFR5cGU0IjoiaHNsKDE3NiwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGU1IjoiaHNsKC00LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkiLCJmaWxsVHlwZTYiOiJoc2woOCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGU3IjoiaHNsKDE4OCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIn19LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ2FudHRcbiAgICB0aXRsZSBCU2MgQ2NvbXB1dGVyIFNjaWVuY2UgR2FudHQgQ2hhcnRcbiAgICBkYXRlRm9ybWF0ICBZWVlZLU1NLUREXG4gICAgc2VjdGlvbiBBcHBsaWNhdGlvbiBvZiBDU1xuICAgIEFydGlmaWNpYWwgSW50ZWxsaWdlbmNlICA6MjAyMC0wOS0yOCwgMzVkXG4gICAgT25saW5lIFRlc3QgMSAgICAgOiAyMDIwLTEwLTI2ICwgMTRkXG4gICAgQ29tcHV0ZXIgVmlzaW9uICA6IDIwMjAtMTEtMDksIDM1ZFxuICAgIE9ubGluZSBUZXN0IDIgICAgIDogMjAyMC0xMi0wNywgMTRkXG4gICAgUm9ib3RpY3MgYW5kIFZpcnR1YWwgUmVhbGl0eTogMjAyMS0wMS0xMSAsIDM1ZFxuICAgIFN1bW1hdGl2ZSBBc2Vzc21lbnQgVEJBOiBcbiAgICBEYXRhIEFuYWx5dGljczogMjAyMS0wMi0yMiAsIDM1ZFxuICAgIFN1bW1hdGl2ZSBBc2Vzc21lbnQgVEJBOiBcbiBcbiAgICBzZWN0aW9uIEZ1bmRhbWVudGFscyBvZiBDU1xuICAgIEluZm9ybWF0aW9uIFJlcHJlc2VudGF0aW9uIGFuZCBEYXRhIG1hbmlwdWxhdGlvbiAgOjIwMjAtMDktMjgsIDM1ZFxuICAgIE9ubGluZSBUZXN0IDEgICAgIDogMjAyMC0xMC0yNiAsIDFkXG4gICAgT1MgYW5kIENvbXB1dGVyIE5ldHdvcmtzIDogMjAyMC0xMS0wOSwgMzVkXG4gICAgTGFiIFJlcG9ydCAxICAgICAgOiAyMDIwLTExLTA5LCAyNmRcbiAgICBPbmxpbmUgVGVzdCAyICAgICA6IDIwMjAtMTItMDcsIDFkICAgIGFub3RoZXIgdGFzayAgICAgIDogMjRkXG4gICAgSW5mb3JtYXRpb24gU2VjdXJpdHkgJiBBbGdvcml0aG1zOiAyMDIxLTAxLTExICwgMzVkXG4gICAgUHJvZ3JhbW1pbmcgbGFuZ3VhZ2U6IDIwMjEtMDItMjIgLCAzNWRcbiAgICBMYWIgUmVwb3J0IDIgOiAyMDIxLTAyLTIyLCAyNmQgICBcbiAgICBcblxuXG4gICAgc2VjdGlvbiBNYXRocyBhbmQgQ29tcHV0aW5nXG4gICAgQWxnZWJyYSwgRnVuY3Rpb25zLCBHcmFwaHMgIDoyMDIwLTA5LTI4LCAzNWRcbiAgICBNYXRyaWNlcyBhbmQgdmVjdG9ycyA6IDIwMjAtMTEtMDksIDM1ZFxuICAgIExhYiBSZXBvcnQgMSAgICAgIDogMjAyMC0xMC0yOCwgNDJkXG4gICAgQ2FsY3VsdXM6IDIwMjEtMDEtMTEgLCAzNWRcbiAgICBDb21wbGV4IG51bWJlcnMsIFByb2IuICYgU3RhdHM6IDIwMjEtMDItMjIgLCAzNWRcbiAgICBMYWIgUmVwb3J0IDIgOiAyMDIxLTAxLTI4LCA0MmQgICAgICAgIFxuXG5cbiAgICBzZWN0aW9uIFByb2dyYW1taW5nIFxuICAgIEZ1bmRhbWVudGFscyBvZiBwcm9ncmFtbWluZyBhbmQgdGhlIEMtbGFuZ3VhZ2UgIDoyMDIwLTA5LTI4LCAzNWRcbiAgICBPbmxpbmUgVGVzdCAxICAgICA6IDIwMjAtMTAtMjYgLCAxZFxuICAgIEZ1bmRhbWVudGFscyBvZiBwcm9ncmFtbWluZyBhbmQgdGhlIEMtbGFuZ3VhZ2UgOiAyMDIwLTExLTA5LCAzNWRcbiAgICBEZXZlbG9wbWVudCBQcm9qZWN0IDogMjAyMC0xMS0wOSwgMzVkXG4gICAgT25saW5lIFRlc3QgMiAgICAgOiAyMDIwLTEyLTExLCAxZCAgICBcbiAgICBPYmplY3Qtb3JpZW50ZWQgcHJvZ3JhbW1pbmcgYW5kIHRoZSBDKysgbGFuZ3VhZ2U6IDIwMjEtMDEtMTEgLCAzNWRcbiAgICBPYmplY3Qtb3JpZW50ZWQgcHJvZ3JhbW1pbmcgYW5kIHRoZSBDKysgbGFuZ3VhZ2U6IDIwMjEtMDItMjIgLCAzNWRcbiAgICBPbmxpbmUgVGVzdCAzIDogMjAyMS0wMy0yNiwgMWQgICBcbiAgICBcbiAgICBzZWN0aW9uIFNvZnR3YXJlIEVuZ2luZWVyaW5nXG4gICAgSW50cm8sIEZvdW5kYXRpb24gYW5kIFF1YWxpdHkgZHJpdmVycywgIDoyMDIwLTA5LTI4LCAzNWRcbiAgICBDb3Vyc2UgV29yayAxOiAyMDIwLTEwLTA5LCAxNGQgXG4gICAgU0UgTW9kZWxzICYgTWV0aG9kcywgUHJvY2Vzc2VzICYgTWFuYWdlbWVudCwgYW5kIFJlcXVpcmVtZW50cyA6IDIwMjAtMTEtMDksIDM1ZFxuICAgIENvdXJzZSBXb3JrIDIgOiAyMDIwLTEwLTMwLCA0MmQgXG4gICAgVGVzdGluZywgRGVzaWduOiAyMDIxLTAxLTExICwgMzVkXG4gICAgQ29uc3RydWN0aW9uLCBNYWludGVuYW5jZSBhbmQgRGVjb21taXNzaW9uaW5nOiAyMDIxLTAyLTIyICwgMzVkXG4gICAgU3VtbWF0aXZlIEFzZXNzbWVudCBUQkE6IGAgICAgXG5cbiAgICBzZWN0aW9uIFN0dWRlbnQgRW50ZXJwcmlzZVxuICAgIEVudHJlcHJlbmV1cmlhbCBtaW5kLXNldCBhbmQgcHJvY2VzcyAgOjIwMjAtMDktMjgsIDdkXG4gICAgSWRlYXRpb24gYW5kIGRlc2lnbiB0aGlua2luZyA6IDIwMjAtMTAtMDUsIDdkXG4gICAgRWZmZWN0dWF0aW9uIDogMjAyMC0xMC0xMiwgN2QgXG4gICAgQnVzaW5lc3MgbW9kZWxzLCBCdXNpbmVzcyBtb2RlbCBjYW52YXMsIGJ1c2luZXNzIG1vZGVsIHBhdHRlcm5zOiAyMDIwLTEwLTE5ICwgN2RcbiAgICBWaWFiaWxpdHksIGRlc2lyYWJpbGl0eSBhbmQgZmVhc2liaWxpdHk6IDIwMjAtMTAtMjYgLCA3ZFxuICAgIExlYW4gc3RhcnQtdXAsIHZhbGlkYXRpb24gZXhwZXJpbWVudHMsIGN1c3RvbWVyIHJlc2VhcmNoOiAyMDIwLTExLTA5XG4gICAgUHJvamVjdC1tYW5hZ2VtZW50IGFuZCB0ZWFtLW1hbmFnZW1lbnQ6IDIwMjAtMTEtMTZcbiAgICBDYXNoZmxvdywgY29zdCAmIHByb2ZpdCwgcHJpY2luZyBzdHJhdGVnaWVzOiAyMDIwLTExLTIzXG4gICAgUHJlc2VudGluZyB0aGUgYnVzaW5lc3MgaWRlYSA6IDIwMjAtMTEtMzBcbiAgICBGaW5hbmNpbmcgZm9yIHN0YXJ0LXVwczogMjAyMC0xMi0wN1xuXG4gICAgRGV2ZWxvcCBCdXNpbmVzcyBQbGFuOiAyMDIwLTEwLTE2LCAxMjBkYFxuICAgIEV4YW0gOiAyMDIxLTAxLTE4LCAxZCBcblxuICAgIFxuICAgIHNlY3Rpb24gRXhhbVxuICAgIEV4YW0gcGVyaW9kIDogMjAyMS0wNS0xMCwgNDVkXG4gICAgVGhlIGZpcnN0IGZpdmUgbW9kdWxlIGFyZSBhc3Nlc3NlZCBpbiB0aGlzIHBlcmlvZDogLlxuICAgIFNwZWNpZmljIGV4YW0gZGF0ZXMgYXJlIFRCQTogLiBcbiIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0IiwidGhlbWVWYXJpYWJsZXMiOnsiYmFja2dyb3VuZCI6IndoaXRlIiwicHJpbWFyeUNvbG9yIjoiI0VDRUNGRiIsInNlY29uZGFyeUNvbG9yIjoiI2ZmZmZkZSIsInRlcnRpYXJ5Q29sb3IiOiJoc2woODAsIDEwMCUsIDk2LjI3NDUwOTgwMzklKSIsInByaW1hcnlCb3JkZXJDb2xvciI6ImhzbCgyNDAsIDYwJSwgODYuMjc0NTA5ODAzOSUpIiwic2Vjb25kYXJ5Qm9yZGVyQ29sb3IiOiJoc2woNjAsIDYwJSwgODMuNTI5NDExNzY0NyUpIiwidGVydGlhcnlCb3JkZXJDb2xvciI6ImhzbCg4MCwgNjAlLCA4Ni4yNzQ1MDk4MDM5JSkiLCJwcmltYXJ5VGV4dENvbG9yIjoiIzEzMTMwMCIsInNlY29uZGFyeVRleHRDb2xvciI6IiMwMDAwMjEiLCJ0ZXJ0aWFyeVRleHRDb2xvciI6InJnYig5LjUwMDAwMDAwMDEsIDkuNTAwMDAwMDAwMSwgOS41MDAwMDAwMDAxKSIsImxpbmVDb2xvciI6IiMzMzMzMzMiLCJ0ZXh0Q29sb3IiOiIjMzMzIiwibWFpbkJrZyI6IiNFQ0VDRkYiLCJzZWNvbmRCa2ciOiIjZmZmZmRlIiwiYm9yZGVyMSI6IiM5MzcwREIiLCJib3JkZXIyIjoiI2FhYWEzMyIsImFycm93aGVhZENvbG9yIjoiIzMzMzMzMyIsImZvbnRGYW1pbHkiOiJcInRyZWJ1Y2hldCBtc1wiLCB2ZXJkYW5hLCBhcmlhbCIsImZvbnRTaXplIjoiMTZweCIsImxhYmVsQmFja2dyb3VuZCI6IiNlOGU4ZTgiLCJub2RlQmtnIjoiI0VDRUNGRiIsIm5vZGVCb3JkZXIiOiIjOTM3MERCIiwiY2x1c3RlckJrZyI6IiNmZmZmZGUiLCJjbHVzdGVyQm9yZGVyIjoiI2FhYWEzMyIsImRlZmF1bHRMaW5rQ29sb3IiOiIjMzMzMzMzIiwidGl0bGVDb2xvciI6IiMzMzMiLCJlZGdlTGFiZWxCYWNrZ3JvdW5kIjoiI2U4ZThlOCIsImFjdG9yQm9yZGVyIjoiaHNsKDI1OS42MjYxNjgyMjQzLCA1OS43NzY1MzYzMTI4JSwgODcuOTAxOTYwNzg0MyUpIiwiYWN0b3JCa2ciOiIjRUNFQ0ZGIiwiYWN0b3JUZXh0Q29sb3IiOiJibGFjayIsImFjdG9yTGluZUNvbG9yIjoiZ3JleSIsInNpZ25hbENvbG9yIjoiIzMzMyIsInNpZ25hbFRleHRDb2xvciI6IiMzMzMiLCJsYWJlbEJveEJrZ0NvbG9yIjoiI0VDRUNGRiIsImxhYmVsQm94Qm9yZGVyQ29sb3IiOiJoc2woMjU5LjYyNjE2ODIyNDMsIDU5Ljc3NjUzNjMxMjglLCA4Ny45MDE5NjA3ODQzJSkiLCJsYWJlbFRleHRDb2xvciI6ImJsYWNrIiwibG9vcFRleHRDb2xvciI6ImJsYWNrIiwibm90ZUJvcmRlckNvbG9yIjoiI2FhYWEzMyIsIm5vdGVCa2dDb2xvciI6IiNmZmY1YWQiLCJub3RlVGV4dENvbG9yIjoiYmxhY2siLCJhY3RpdmF0aW9uQm9yZGVyQ29sb3IiOiIjNjY2IiwiYWN0aXZhdGlvbkJrZ0NvbG9yIjoiI2Y0ZjRmNCIsInNlcXVlbmNlTnVtYmVyQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvciI6InJnYmEoMTAyLCAxMDIsIDI1NSwgMC40OSkiLCJhbHRTZWN0aW9uQmtnQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvcjIiOiIjZmZmNDAwIiwidGFza0JvcmRlckNvbG9yIjoiIzUzNGZiYyIsInRhc2tCa2dDb2xvciI6IiM4YTkwZGQiLCJ0YXNrVGV4dExpZ2h0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0RGFya0NvbG9yIjoiYmxhY2siLCJ0YXNrVGV4dE91dHNpZGVDb2xvciI6ImJsYWNrIiwidGFza1RleHRDbGlja2FibGVDb2xvciI6IiMwMDMxNjMiLCJhY3RpdmVUYXNrQm9yZGVyQ29sb3IiOiIjNTM0ZmJjIiwiYWN0aXZlVGFza0JrZ0NvbG9yIjoiI2JmYzdmZiIsImdyaWRDb2xvciI6ImxpZ2h0Z3JleSIsImRvbmVUYXNrQmtnQ29sb3IiOiJsaWdodGdyZXkiLCJkb25lVGFza0JvcmRlckNvbG9yIjoiZ3JleSIsImNyaXRCb3JkZXJDb2xvciI6IiNmZjg4ODgiLCJjcml0QmtnQ29sb3IiOiJyZWQiLCJ0b2RheUxpbmVDb2xvciI6InJlZCIsImxhYmVsQ29sb3IiOiJibGFjayIsImVycm9yQmtnQ29sb3IiOiIjNTUyMjIyIiwiZXJyb3JUZXh0Q29sb3IiOiIjNTUyMjIyIiwiY2xhc3NUZXh0IjoiIzEzMTMwMCIsImZpbGxUeXBlMCI6IiNFQ0VDRkYiLCJmaWxsVHlwZTEiOiIjZmZmZmRlIiwiZmlsbFR5cGUyIjoiaHNsKDMwNCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGUzIjoiaHNsKDEyNCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIiwiZmlsbFR5cGU0IjoiaHNsKDE3NiwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGU1IjoiaHNsKC00LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkiLCJmaWxsVHlwZTYiOiJoc2woOCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGU3IjoiaHNsKDE4OCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIn19LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

Figure 2: Gantt Chart

The Gantt chart represents the schedule for the first year of my studies. The modules start in September 2020 and runs until the end of spring, except for the Student Enterprise module. This is a 20-credit module, where the lectures are taught in the autumn term alone, and the assessments are conducted at the start of the spring term. 
The remaining modules contain formative and summative assessments that are conducted during the term time, for example, in the Applications of CS module, we have summative assessments at the end of each term. Similarly, each one of the module is assessed either through course works or through online tests during the term time. 
The formative and summative assessments are dependent on the the lecture material, when a topic is taught, it is initially assessed through formative tests to help us with understanding the module, it is then assessed through summative assessment at the end of each term or half term, when usually a topic thats being studied has ended. 

The modules that are taught through out the year are examined in May/June, the specific exam dates are published near the beginning of the exam period. 

Dependencies 

The topics taught in each module are inter dependant on other modules, which will support our understanding in different areas. The mathematics and computation module will provide the mathematical understandings and the use of computing to solve mathematical problems, which is a useful skill as its applied in all the modules. Mathematics has various uses to represent and manipulate data. This module provides us with theskills to be able to apply mathematical techniques using computers.
Programming provides the essential skills in computer science, as programming is used in modules such as Mathematics, Software Engineering, Applications of CS and Fundamentals of CS.  These modules are dependent on programming, as we need to have a good understanding in programming, in order to develop a software (for example). 
The Applications of CS module provides us with the skills in order to be able to dcribe typical techniques and to be able to apply the relavent algorithms to artificial intellegence and robots; and to be albe to use basic algorithms describing tasksinvolved in computer vision and computer graphics and to be able to manage workflows with the relavent data analytical tools.
The Fundamentals of CS provides a foundation knowledge for computer scienceI It focuses on the main features of computer systems and algorithms, these studies are relavent for further modules in the course, as the fundametal knowledge will enable us to develop our knowledge further.
Software Engineering provides an introdcution to the concept, it then leads to the practice and management of software engineering. It includes the lifecycle activites in devloping software and it includes the management activities that are requiredto develop a software which is in line with the constraints, such as time and buget, and it meets the design requirements. The module includes case studies and example to represent the application of the concepts and principles to real-world systems
The Student Enterprise is an interactive and practical module that provides an introduction to the key concepts of business start-up. Having entrepreneurial skills is useful as well apply many of the skills in other areas, such as communication, which can be developed to a high standard through this module. 

### Schedule

![image_1.png](./image_1.png)

Table 1: Overall study hours

![image_2.png](./image_2.png)

Table 2: Weekly independent study hours

![image_3.png](./image_3.png)

Table 3: Independent study weekly schedule

The advised independent study time is included in table 1: One average we must spend the recommended time on each module, thus I have created the table 2 to divide the time into weekly independent study time. Then I created the weekly schedule to study each module based on the recommended study time. 
The independent weekly study schedule will enable me to be on track with my studies, through this method I will be able to attend the lectures, view the online lectors, complete my independent study, and to complete the course works on time. 


## Reflection
The assignment has been very beneficial for me as it was a chance to look into the modules that is to be taught in the current year, as well as the future years to come. It enabled me to see how different components are inter-connected and what each module will teach us. 
This assignment further clarified my current learning plan. It supported me to see the breakdown of the modules that clarified what is being taught in each module and where it will lead in the future. The WBS is a simple chart and it’s easy to create using PowerPoint, or the mermaid syntax, but for larger projects it is difficult to create using these two methods as in PowerPoint many boxes needs to be created and manually connected which is very time consuming. The mermaid syntax is very easy to use, and it generates the WBS automatically, it produces neat results for small projects, but for larger projects it becomes difficult to view. I also used PlantUML, it produced the WBS as I wanted to, however due to restricted access to the Gitlab Admin Area I was unable to integrate PlantUML into Gitlab.  
I may have included more information in the WBS that its required, however it is a good practice for me as it was a good practice.   

Creation Gantt chart was very useful as I could see the dependencies and milestones of each module. This will help me to be better prepared for my studies as I can make use of the schedule to see what would be coming ahead in the upcoming weeks. Creating a Gantt chart is not very complicated as we enter the task and, the start and end date. However, we need to be cautious to include the exact dates as it can cause clashes with our work.  A Gantt chart for the entire degree programme requires information for the key dates for the upcoming years, I have only produced the Gannt chart for the first year and I will be produce one at the beginning of each year in the future. The milestones that I included are the summative assessments, it would have been useful to include the formative assessments as well as it could count as a milestone. 

The weekly schedule for independent study will support me to be on track with my studies, as I can study in small sections to cover the entire module contents.  Covering the entire module contents will be useful as I can obtain a good understanding in each area of the module. The schedule is for the autumn term and in each term, I should create one, as the timetables change for some modules. 

## Reference:
[1] Uefap.com. (2019). Genres in Academic Writing: Reflective writing. [online] Available at: http://www.uefap.com/writing/genre/reflect.htm.

[2] mermaid-js.github.io. (n.d.). Mermaid live editor. [online] Available at: https://mermaid-js.github.io/mermaid-live-editor/ [Accessed 12 Nov. 2020].

[3] www.reading.ac.uk. (n.d.). Modules - University of Reading. [online] Available at: http://www.reading.ac.uk/modules/module.aspx?sacyr=1819&school=MPS [Accessed 12 Nov. 2020].

[4] docs.gitlab.com. (n.d.). GitLab Markdown | GitLab. [online] Available at: https://docs.gitlab.com/ee/user/markdown.html [Accessed 12 Nov. 2020].

[5] Chambers.com.au. (2020). Baseline | Configuration Management | Software Engineering | Systems Engineering. [online] Available at: https://www.chambers.com.au/glossary/baseline.php.

[6] Reading.ac.uk. (2020). [online] Available at: http://www.reading.ac.uk/progspecs/specifications.aspx?type=UGS&year=2021&id=MPS [Accessed 12 Nov. 2020].

[7] Wikipedia Contributors (2019). Work breakdown structure. [online] Wikipedia. Available at: https://en.wikipedia.org/wiki/Work_breakdown_structure.
## Appendix

### CV 
```
Ajmal Sangar
40 High Road
Willesden
London
NW10 0AA
Mobile: 07473437019
Email: A.Sangar@student.reading.ac.uk
```
##### Education
```
September 2020  -   June 2024      Reading University          BSc(Hons) Computer Science with Industrial Year

September 2013  -   June 2014      Kingston University         Foundation Degree in Aerospace Engineering

September 2010  -   June 2013      Hackney Community College    GSCEs and A-Level
- A-Levels: Maths, Further Maths, Physics
- GSCEs: 2As, 2Bs and C in core subjects
```

##### Engineering Projects
|  | Engineering Projects | Skills Applied and Obtained |
| ---      |  ------  |----------|
| 1 | DNS of Airflow Separation over an Aerofoil: Conducted a parametric study using DNS to analyse flow separation and implemented filaments to delay flow separation and consequently the aerodynamic stall.| This project developed my research and analytical skills, as I studied similar researches conduced on this phenomenon.    |
| 2 | Conceptual Design of an Aircraft: Developed a conceptual design of an aircraft in accordance with the requirements and in compliance with EASA CS-25 regulations. Tasks Conducted: Research into historical aircraft data; produced a constraints diagram to analyse the aerodynamic forces, determined the wing, tail and fin planform, calculated the center of gravity, and produced a CAD design.|I initially research into current and historical aircrafts, secondly, I produced a constraint diagram to determine suitable design parameters that required mathematic manipulation and analysis, subsequently I produced the conceptual design in compliance with EASA CS-25 regulations.  |
| 3 | Turboshaft Engine Design: Produced a conceptual design of a turboshaft engine to power a light-weight helicopter. Responsibilities: Conducted cycle analysis, analysed design parameters for the turbines, determined the number of turbine stages, analysed blade profile to maintain the air flow subsonic and efficient. |Time Management: This was a two-month module, during the limited time I studied and applied my knowledge to develop the engine design. Team-work: I ensured the turbines were suitable for the design parameters and it will function with other engine components, such as the compressor and combustor.  |
| 4 | EGPR – Interdisciplinary Collaborative Design Project Project 2 – Developed a mobile illuminating product for Black Diamond company, in collaboration with mechanical, electrical, and industrial design engineers from Budapest University of Technology and Economics (Hungary) and Brigham Young University (USA). Responsibilities: project secretary, designed the mechanical system, developed 3D models, analysed the functionality/practicality of the design. Project 1- Developed a conceptual design for an automated dispenser in collaboration with industrial design engineers from Strathclyde University. Responsibilities: project secretary, designed the mechanical system, and produced technical drawings.  |Innovation: Developed many concepts and selected those that met the project requirements. Communication and Teamwork: Communicated with colleagues from different universities, produce system components at different locations and the prototype was assembled at City University of London Presentation: Presented outcomes at different phases of the project. Understood the commercial aspect of product design. |
| 5 | Business Plan: Produced a business plan for an airline in compliance with the regulatory authorities. Responsibilities: Legal Officer, Market Researcher, Crisis Manager, Airworthiness of Aircraft and Safety Management.  |Decisiveness: Following the market research, I suggested to my team with analysis to target our airline towards developing countries.  |

##### Employment
```
July 2018 – September 2020                          Self-Employed 		Driver
-	Developed my people’s skills through this job

August 2014 – November 2016 (Part Time)             Assistant Store Manager	Londis
-	Managed the stock of two stores
-	Checked current stock and made new orders
-	Managed a small group and their shift hours
```

##### Technical Skills
```
Programming language: C, C++, Java                      Bash
Gitlab                                                  MATLAB
MS Windows, Apple Mac and Linux operating systems       Microsoft Office Suite
AutoCAD                                                 Ansys Fluent
SolidWorks                                              Ansys Mechanical
Microsoft Project                                       XFLR-5
PointWise 360                                           TechPlot
```

##### Soft Skills
```
Team/Group work
Communication
Time-Management
Presentation
Decisiveness
Research
Analytical skills
Mathematics and Computing
```
##### Interest

I have keen interest in computer programming and I regularly practice my programming skills. I regularly read about the new developments in computing and engineering in order to remain up to date with the new developments in the industry. 

##### Reference
Available upon request














